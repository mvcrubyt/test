# talisman-demo

## Blog Link 

[Running Talisman CLI in the GitLab CI Servers](https://medium.com/@PraveenMathew92/running-talisman-cli-in-the-gitlab-ci-servers-29f15af7b1c7)

## Branch with no credentials checked in
[master](https://gitlab.com/PraveenMathew92/talisman-demo/-/tree/master)

### Pipeline
[#165371408](https://gitlab.com/PraveenMathew92/talisman-demo/-/pipelines/165371408)

## Branch with credentials checked in
[sensitive-information-talisman-scan](https://gitlab.com/PraveenMathew92/talisman-demo/-/tree/sensitive-information-talisman-scan)

### Pipeline
[#165366316](https://gitlab.com/PraveenMathew92/talisman-demo/-/pipelines/165366316)

## Branch with html reporting
[sensitive-information-talisman-html-reporting](https://gitlab.com/PraveenMathew92/talisman-demo/-/tree/sensitive-information-talisman-html-reporting)

### Pipeline
[#165366364](https://gitlab.com/PraveenMathew92/talisman-demo/-/pipelines/165366364)